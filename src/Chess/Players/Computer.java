package Chess.Players;

import Chess.Color;
import Chess.Move;
import jdk.jshell.spi.ExecutionControl;

public class Computer extends Player {
    public Computer(Color color) {
        super("Computer", color);
    }
    @Override
    public Move makeChoose() throws ExecutionControl.NotImplementedException {
        throw new ExecutionControl.NotImplementedException("We are not ready for this");
    }
}
package Chess.Figures;

import Chess.AllowableMoves;
import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;

import java.awt.*;

// Слон ходит по диагоналям
public class Bishop extends Piece {
    public Bishop(Color color) {
        super("\u265D", color);
    }

    @Override
    public boolean canMove(Move move) {
        return iMove( move );
    }

    public static boolean iMove(Move move) {
        Piece lhs = chessBoard.getPiece(move.from);
        Piece rhs = chessBoard.getPiece(move.to);
        if ( rhs != null && lhs.color == rhs.color ) return false;
        Point diff = move.way();
        boolean canMove = Math.abs(diff.x) == Math.abs(diff.y);
        if (canMove) {
            boolean isWay = isWayEmpty( move );
            return isWay;
        }
        return false;
    }
}
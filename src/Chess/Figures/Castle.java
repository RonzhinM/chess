package Chess.Figures;

import Chess.AllowableMoves;
import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;

import java.awt.*;

// Ладья ходит по вертикали и горизонтали
public class Castle extends Piece {
    public Castle(Color color) {
        super("\u265C", color);
    }

    @Override
    public boolean canMove(Move move) {
        return iMove( move );
    }

    public static boolean iMove(Move move) {
        Piece lhs = chessBoard.getPiece(move.from);
        Piece rhs = chessBoard.getPiece(move.to);
        Point diff = move.way();
        int x = Math.abs(diff.x);
        int y = Math.abs(diff.y);
        boolean canMove = x >= 1 && y == 0 || x == 0 && y >= 1;
        if ( !canMove ) { return false; }
        if ( AllowableMoves.CastleVSKing.check(move, chessBoard) && isWayEmpty(move)) return true;
        return isWayEmpty( move ) && (rhs == null || rhs.color != lhs.color);
    }
}
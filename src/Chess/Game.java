package Chess;

import Chess.Figures.Piece;
import Chess.Players.Player;
import Chess.Players.Players;
import jdk.jshell.spi.ExecutionControl;

import java.awt.*;
import java.util.Map;

public class Game {
    private Players players;
    private ChessBoard chessBoard;
    {
        chessBoard = new ChessBoard();
    }

    public Game(Player pl1, Player pl2) {
        if (pl1.color == pl2.color) throw new IllegalArgumentException("The colors can not be equals");
        pl1.setChessBoard(chessBoard);
        pl2.setChessBoard(chessBoard);
        this.players = new Players(pl1, pl2);
        chessBoard.show();
    }

    public void run() throws ExecutionControl.NotImplementedException {
        while ( true ) {
            Player player = players.nextPlayer();
            Move move = player.makeChoose();
            chessBoard.move( move );
            while ( chessBoard.isCheck(player.color) ) {
                chessBoard.previousMove();
                if ( chessBoard.isCheckMate( player ) ) {
                    System.out.println("The winner is " + players.nextPlayer());
                    return;
                }
                System.out.println("The check. Choose again " + player);
                move = player.makeChoose();
                chessBoard.move(move);
            }
            clearScreen();
            chessBoard.show();
        }
    }

    public void clearScreen() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                System.out.println("\033\143");
                //Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e) {
            System.out.println("не получилось очистить");
        }
    }
}
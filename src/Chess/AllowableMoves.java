package Chess;

import Chess.Figures.Castle;
import Chess.Figures.King;
import Chess.Figures.Pawn;
import Chess.Figures.Piece;

public enum AllowableMoves {
    PawnTransform {
        @Override
        public boolean check(Move move, ChessBoard chessBoard) {
            Piece lhs = chessBoard.getPiece(move.from);
            int currentPos = lhs.color == Color.WHITE ? 7 : 0;
            if (move.from.y == currentPos) return false;
            if ( lhs instanceof Pawn )
                return lhs.color == Color.WHITE ? move.to.y == 7 : move.to.y == 0;
            return false;
        }
    },
    PawnEat {
        @Override
        public boolean check(Move move, ChessBoard chessBoard) {
            Piece lhs = chessBoard.getPiece(move.from);
            Piece rhs = chessBoard.getPiece(move.to);
            if ( rhs == null || lhs.color == rhs.color ) return false;
            int directiorn = lhs.color == Color.WHITE ? 1 : -1;
            return Math.abs(move.way().x) == 1 && move.way().y == directiorn;
        }
    },
    CastleVSKing {
        @Override
        public boolean check(Move move, ChessBoard chessBoard) {
            Piece rhs = chessBoard.getPiece(move.to);
            Piece lhs = chessBoard.getPiece(move.from);
            if ( !lhs.isFirst() && !rhs.isFirst()) return false;
            if (lhs instanceof King && rhs instanceof Castle) return true;
            if (rhs instanceof King && lhs instanceof Castle) return true;
            return false;
        }
    },
    KingByAttake {
        @Override
        public boolean check(Move move, ChessBoard chessBoard) { Piece rhs = chessBoard.getPiece(move.to);
            return rhs instanceof King;
        }
    };
    public abstract boolean check(Move move, ChessBoard chessBoard);
}
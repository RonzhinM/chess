package Chess.Figures;

import Chess.AllowableMoves;
import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;

import java.awt.*;

// Конь ходит буквой Г
public class Knight extends Piece {
    public Knight(Color color) {
        super("\u265E", color);
    }

    @Override
    public boolean canMove(Move move) {
        Piece lhs = chessBoard.getPiece(move.from);
        Piece rhs = chessBoard.getPiece(move.to);
        if ( rhs != null && lhs.color == rhs.color ) return false;
        Point diff = move.way();
        int x = Math.abs(diff.x);
        int y = Math.abs(diff.y);
        return x == 2 && y == 1 || y == 2 && x == 1;
    }
}
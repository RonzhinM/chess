package Chess.UndoRedo;

import Chess.ChessBoard;
import Chess.Figures.King;
import Chess.Figures.Piece;
import Chess.Move;

import java.awt.*;

public class CastlingCommand implements ICommand {
    private final Move move;
    private final Piece lhs;
    private final Piece rhs;
    private final ChessBoard chessBoard;
    public CastlingCommand(Move move, ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
        this.move = move;
        this.lhs = chessBoard.getPiece(move.from);
        this.rhs = chessBoard.getPiece(move.to);
        Execute();
    }

    @Override
    public void Execute() {
        Point king = new Point(Math.abs(move.way().x) == 3 ? 1 : 5, move.from.y);
        Point castle = new Point(Math.abs(move.way().x) == 3 ? 2 : 4, move.from.y);
        chessBoard.setPiece(null, move.from);
        chessBoard.setPiece(null, move.to);
        if (lhs instanceof King) {
            chessBoard.setPiece(lhs, king);
            chessBoard.setPiece(rhs, castle);
        }
        else {
            chessBoard.setPiece(rhs, king);
            chessBoard.setPiece(lhs, castle);
        }
    }

    @Override
    public void UnExecute() {
        Point king = new Point(Math.abs(move.way().x) == 3 ? 1 : 5, move.from.y);
        Point castle = new Point(Math.abs(move.way().x) == 3 ? 2 : 4, move.from.y);
        chessBoard.setPiece(null, king);
        chessBoard.setPiece(null, castle);
        if (lhs instanceof King) {
            chessBoard.setPiece(lhs, move.to);
            chessBoard.setPiece(rhs, move.from);
        }
        else {
            chessBoard.setPiece(rhs, move.to);
            chessBoard.setPiece(lhs, move.from);
        }
    }
}

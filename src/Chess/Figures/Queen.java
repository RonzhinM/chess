package Chess.Figures;

import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;

import java.awt.*;

public class Queen extends Piece {
    public Queen(Color color) {
        super("\u265B", color );
    }

    @Override
    public boolean canMove(Move move) {
        return Bishop.iMove(move) || Castle.iMove(move);
    }
}
package Chess.UndoRedo;

import Chess.ChessBoard;
import Chess.Figures.*;
import Chess.Move;

import java.util.Scanner;

public class PromotionCommand implements ICommand {
    private final Move move;
    private final Piece lhsBefore;
    private final Piece lhsAfter;
    private final Piece rhs;
    private final ChessBoard chessBoard;

    public PromotionCommand(Move move, ChessBoard chessBoard) {
        this.move = move;
        this.chessBoard = chessBoard;
        this.lhsBefore = chessBoard.getPiece(move.from);
        this.rhs = chessBoard.getPiece(move.to);
        this.lhsAfter = chooseNewFigure();
        Execute();
    }
    private Piece chooseNewFigure() {
        System.out.println("Make your choose: 1 - Pawn. " +
                "2 - Bishop. " + "3 - Castle. " + "4 - Knight. " + "5 - Queen.");
        try {
            switch (new Scanner(System.in).nextInt()) {
                case 1: return new Pawn(lhsBefore.color);
                case 2: return new Bishop(lhsBefore.color);
                case 3: return new Castle(lhsBefore.color);
                case 4: return new Knight(lhsBefore.color);
                case 5: return new Queen(lhsBefore.color);
            }
        }
        catch (Exception ex) {}
        return chooseNewFigure();
    }

    @Override
    public void Execute() {
        chessBoard.setPiece( lhsAfter, move.to );
    }

    @Override
    public void UnExecute() {
        chessBoard.setPiece(lhsBefore, move.from);
        chessBoard.setPiece(rhs, move.to);
    }
}

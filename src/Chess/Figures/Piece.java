package Chess.Figures;

import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;

import java.awt.*;
import java.io.Serializable;

public abstract class Piece implements Serializable {
    // fields
    private final String name;
    private boolean isFirstStep = true;
    public final Color color;
    protected static ChessBoard chessBoard;
    public void setChessBoard(ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
    }

    // Constructor
    public Piece(String name, Color color) {
        this.name = name;
        this.color = color;
    }

    // public methods
    public void state() { isFirstStep = false; }
    public boolean isFirst() { return isFirstStep; }

    public abstract boolean canMove(Move move);
    public static boolean isWayEmpty(Move move) {
        int fromY = move.way().y == 0 ? 0 : move.fromY() + 1;
        int fromX = move.way().x == 0 ? 0 : move.fromX() + 1;
        int toY = move.way().y == 0 ? 0 : move.toY();
        int toX = move.way().x == 0 ? 0 : move.toX();
        for (int x = fromX, y = fromY; x < toX || y < toY; ) {
            if ( chessBoard.isPiece( new Point(x, y) ) ) return false;
            if ( move.way().x != 0  ) x++;
            if ( move.way().y != 0  ) y++;
        }
        return true;
    }
    public boolean equals( Object figure ) {
        if (figure == null) throw new NullPointerException("The input figure is null");
        if ( figure instanceof Piece ) {
            return this.getClass().getSimpleName().equals(figure.getClass().getSimpleName()) &&
                    this.color.equals(((Piece) figure).color
                    );
        }
        return false;
    }
    public String toString() {
        return color + name + Color.DEFAULT;
    }
}
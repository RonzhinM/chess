package Chess.Players;

import Chess.AllowableMoves;
import Chess.Color;
import Chess.Figures.Piece;
import Chess.Move;

import java.awt.*;
import java.io.IOException;
import java.util.Scanner;

public class Human extends Player {
    public Human(String name, Color color) {
        super(name, color);
    }

    @Override
    public Move makeChoose() {
        Point from = chooseFigure(true);
        Point to = chooseFigure(false);
        try {
            if ( from.equals(to)) throw new IllegalArgumentException("You can not move on place where you stand");
            Move move = new Move(from, to);
            Piece piece = chessBoard.getPiece(from);
            boolean canMove = piece.canMove(move);
            boolean isSwap = AllowableMoves.CastleVSKing.check(move, chessBoard);
            boolean isCheck = chessBoard.isCheck( color );
            if (canMove && isCheck && isSwap) throw new IllegalArgumentException("You can not made the castling under the check");
            if (!canMove) throw new IllegalArgumentException("You can not get a new position");
            chessBoard.move(move);
            if ( chessBoard.isCheck(color) ) {
                chessBoard.previousMove();
                throw new IllegalArgumentException("Your move do not save your King");
            }
            chessBoard.previousMove();
            return move;
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return makeChoose();
    }

    private Point chooseFigure(boolean isYours) {
        System.out.print(name + ", choose a " + (isYours ? "piece" : "new position") + ": ");
        try {
            int position = new Scanner(System.in).nextInt();
            if (position < 11 || position > 88) throw new IOException("Position is out of the desk range");
            Point pt = new Point(position / 10 - 1, position % 10 - 1);
            if ( isYours ) {
                if ( chessBoard.isPiece(pt) ) {
                    if (chessBoard.getPiece(pt).color != color) throw new IllegalArgumentException("You must choose your figures");
                    return pt;
                }
                throw new IOException("You must choose a figure");
            }
            return pt;
        }
        catch (Exception ex) {
            System.out.println( ex.getMessage() );
        }
        return chooseFigure(isYours);
    }
}
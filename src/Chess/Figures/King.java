package Chess.Figures;

import Chess.AllowableMoves;
import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;

import java.awt.*;

// король ходит на одну клетку
public class King extends Piece {
    public King(Color color) {
        super("\u265A", color );
    }

    @Override
    public boolean canMove(Move move) {
        Piece lhs = chessBoard.getPiece(move.from);
        Piece rhs = chessBoard.getPiece(move.to);
        Point diff = move.way();
        int x = Math.abs(diff.x);
        int y = Math.abs(diff.y);
        boolean canMove =  x <= 1 && y <= 1;
        if ( canMove && ( rhs == null || rhs.color != lhs.color || AllowableMoves.CastleVSKing.check(move, chessBoard) ) ) return true;
        return false;
    }
}
package Chess.UndoRedo;

import Chess.ChessBoard;
import Chess.Move;
import java.util.Stack;

public class ManagerMoves {
    private Stack<ICommand> undoCommands = new Stack<ICommand>();
    private Stack<ICommand> redoCommands = new Stack<ICommand>();
    private ChessBoard chessBoard;

    public void setChessBoard(ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
    }

    public void redo() {
        if ( redoCommands.empty() ) return;
        ICommand command = redoCommands.pop();
        command.Execute();
    }

    public void undo() {
        if ( undoCommands.empty() ) return;
        ICommand command = undoCommands.pop();
        command.UnExecute();
        redoCommands.push(command);
    }

    public void movePiece(Move move) {
        ICommand cmd = new MoveCommand(move, chessBoard);
        undoCommands.push(cmd);
        redoCommands.clear();
    }

    public void castlingPiece(Move move) {
        ICommand cmd = new CastlingCommand(move, chessBoard);
        undoCommands.push(cmd);
        redoCommands.clear();
    }
    public void promotionPiece(Move move) {
        ICommand cmd = new PromotionCommand(move, chessBoard);
        undoCommands.push(cmd);
        redoCommands.clear();
    }
}
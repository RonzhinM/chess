package Chess.UndoRedo;

import Chess.ChessBoard;
import Chess.Figures.Piece;
import Chess.Move;

public class MoveCommand implements ICommand {
    private final Move move;
    private final Piece lhs;
    private final Piece rhs;
    private final ChessBoard chessBoard;

    public MoveCommand(Move move, ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
        this.move = move;
        this.lhs = chessBoard.getPiece(move.from);
        this.rhs = chessBoard.getPiece(move.to);
        Execute();
    }

    public void Execute() {
        chessBoard.setPiece(lhs, move.to);
        chessBoard.setPiece(null, move.from);
    }

    public void UnExecute() {
        chessBoard.setPiece( rhs, move.to );
        chessBoard.setPiece( lhs, move.from );
    }
}
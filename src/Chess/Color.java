package Chess;

public enum Color {
    BLACK ("\u001B[31m"),
    WHITE ("\u001B[30m"),
    DEFAULT ("\u001B[0m");
    private final String color;
    Color(String color) {
        this.color = color;
    }
    @Override
    public String toString() {
        return color;
    }
}
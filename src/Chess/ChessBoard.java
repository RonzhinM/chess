package Chess;

import Chess.Figures.King;
import Chess.Figures.Piece;
import Chess.Players.Player;
import Chess.UndoRedo.ManagerMoves;

import java.awt.*;
import java.io.*;
import java.util.Map;

public class ChessBoard extends Board implements Serializable, Cloneable {
    private ManagerMoves managerMoves;
    {
        managerMoves = new ManagerMoves();
    }

    ChessBoard() {
        for (Map.Entry<Piece, Point> entry : this) {
            entry.getKey().setChessBoard(this);
        }
        managerMoves.setChessBoard(this);
    }

    public void move(Move move) {
        if ( AllowableMoves.PawnTransform.check(move, this) )
            managerMoves.promotionPiece(move);
        else if ( AllowableMoves.CastleVSKing.check(move, this) )
            managerMoves.castlingPiece(move);
        else managerMoves.movePiece(move);
    }
    public void nexMove () {
        managerMoves.redo();
    }
    public void previousMove () {
        managerMoves.undo();
    }
    public boolean isCheck( Color color ) {
        var king = findKing(color);
        for (Map.Entry<Piece, Point> entry : this) {
            Piece figure = entry.getKey();
            Point pos = entry.getValue();
            if ( figure.color == color ) continue;
            if ( figure.canMove(new Move( pos, king.getValue()) ) ) {
                return true;
            }
        }
        return false;
    }
    public boolean isCheckMate( Player player ) {
        for (Map.Entry<Piece, Point> entry : this) {
            Piece piece = entry.getKey();
            Point from = entry.getValue();
            if (piece.color != player.color) continue;
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    Point to = new Point(x, y);
                    Move move = new Move( from, to );

                    if (piece.canMove(move)) {
                        move(move);
                        if ( !isCheck( player.color )) {
                            previousMove();
                            return false;
                        }
                        previousMove();
                    }
                }
            }
        }
        return true;
    }
    private Map.Entry<Piece, Point> findKing( Color color ) {
        for (Map.Entry<Piece, Point> entry : this) {
            Piece piece = entry.getKey();
            if ( piece.equals(new King(color)) )
                return entry;
        }
        return null;
    }
    @Override
    public ChessBoard clone() {
        // write object into memory (to bytes)
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try(ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(this);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // create new object from memory (from bytes)
        try(ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()))) {
            return (ChessBoard) ois.readObject();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
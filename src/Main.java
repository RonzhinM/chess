import Chess.Color;
import Chess.Game;
import Chess.Players.Human;
import Chess.Players.Player;
import jdk.jshell.spi.ExecutionControl;

public class Main {
    public static void main(String[] args) throws ExecutionControl.NotImplementedException {
        Player pl1 = new Human("Misha", Color.WHITE);
        Player pl2 = new Human("Nina", Color.BLACK);
        new Game(pl1, pl2).run();
    }
}
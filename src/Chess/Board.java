package Chess;

import Chess.Figures.*;

import java.awt.*;
import java.io.Serializable;
import java.util.*;

public abstract class Board implements Serializable, Iterable<Map.Entry<Piece, Point>>  {
    private Piece[][] board;
    public final int size;
    public Board() {
        board = new Piece[][]{
                {new Castle(Color.WHITE), new Knight(Color.WHITE), new Bishop(Color.WHITE), new King(Color.WHITE), new Queen(Color.WHITE), new Bishop(Color.WHITE), new Knight(Color.WHITE), new Castle(Color.WHITE)},
                {new Pawn(Color.WHITE), new Pawn(Color.WHITE), new Pawn(Color.WHITE), new Pawn(Color.WHITE), new Pawn(Color.WHITE), new Pawn(Color.WHITE), new Pawn(Color.WHITE), new Pawn(Color.WHITE)},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {new Pawn(Color.BLACK), new Pawn(Color.BLACK), new Pawn(Color.BLACK), new Pawn(Color.BLACK), new Pawn(Color.BLACK), new Pawn(Chess.Color.BLACK), new Pawn(Color.BLACK), new Pawn(Color.BLACK)},
                {new Castle(Color.BLACK), new Knight(Color.BLACK), new Bishop(Color.BLACK), new King(Color.BLACK), new Queen(Color.BLACK), new Bishop(Chess.Color.BLACK), new Knight(Color.BLACK), new Castle(Color.BLACK)},
        };
        size = board.length;
    }
    public boolean isPiece(Object obj) {
        if (obj instanceof Point) {
            Point pt = (Point)obj;
            return getPiece(pt) != null;
        }
        else if (obj instanceof Piece) {
            return obj != null;
        }
        return false;
    }
    public Piece getPiece(Point pt) {
        return board[pt.y][pt.x];
    }
    public void setPiece(Piece piece, Point position) {
        board[position.y][position.x] = piece;
    }
    public void show() {
        // print vertical designations
        for (int i = 1; i <= size; i++) {
            System.out.print("  " + i + " ");
        }
        System.out.println();
        // first vertical line
        for (int z = 0; z < size * 4 + 1; z++) {
            System.out.print("\u2500");
        }
        // show
        for (int i = 0; i < size; i++) {
            System.out.println();
            for (var piece : board[i]) {
                if (piece == null)
                    System.out.print("| - ");
                else System.out.print("| " + piece + " ");
            }

            // print horizontal designations
            System.out.println("|  " + (i + 1));

            // vertical lines
            for (int z = 0; z < size * 4 + 1; z++) {
                System.out.print("\u2500");
            }
        }
        System.out.println();
    }
    public Iterator<Map.Entry<Piece, Point>> iterator() {
        return new BoardIterator();
    }
    class BoardIterator implements Iterator<Map.Entry<Piece, Point>> {
        private int yp = 0;
        private int xp = -1;
        @Override
        public boolean hasNext() {
            if (xp < 8 - 1) xp++;
            else { yp++; xp = 0; }
            for (int y = yp; y < 8; y++) {
                for (int x = xp; x < 8; x++) {
                    if ( board[y][x] != null ) {
                        yp = y;
                        xp = x;
                        return true;
                    }
                }
                xp = 0;
            }
            return false;
        }
        @Override
        public Map.Entry<Piece, Point> next() {
            return new AbstractMap.SimpleEntry<>(board[yp][xp], new Point(xp, yp));
        }
    }
}
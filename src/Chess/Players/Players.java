package Chess.Players;

public class Players {
    private Player player1;
    private Player player2;
    private int countStep = 0;
    public Players(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    public Player nextPlayer () {
        return countStep++ % 2 == 0 ? player1 : player2;
    }
    public Player previousPlayer () {
        countStep -= 2;
        return nextPlayer();
    }
    public int getCountStep() {
        return countStep;
    }
}
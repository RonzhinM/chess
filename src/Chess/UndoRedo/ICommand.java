package Chess.UndoRedo;

/**
 * Command pattern
 */

public interface ICommand {
    void Execute();
    void UnExecute();
}
package Chess.Figures;

import Chess.AllowableMoves;
import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;

import java.awt.*;
// Пешка ходит на одну клетку вперед
public class Pawn extends Piece {
    public Pawn(Color color) {
        super("\u265F", color);
    }

    @Override
    public boolean canMove(Move move) {
        Piece rhs = chessBoard.getPiece(move.to);
        int y = color == Color.WHITE ? 1 : -1;
        Point diff = move.way();
        boolean canMove = diff.x == 0 && (diff.y == y || diff.y == (isFirst() ? y * 2 : y) );
        if ( canMove ) return isWayEmpty(move) && rhs == null;
        return AllowableMoves.PawnEat.check(move, chessBoard);
    }
}
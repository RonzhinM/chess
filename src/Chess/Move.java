package Chess;

import java.awt.*;

public class Move {
    public final Point from;
    public final Point to;
    public Move(Point from, Point to) {
        this.from = from;
        this.to = to;
    }
    public Point way() { return new Point(to.x - from.x, to.y - from.y); }
    public int toX() { return Integer.max( from.x, to.x ); }
    public int toY() { return Integer.max( from.y, to.y ); }
    public int fromX() { return Integer.min( from.x, to.x ); }
    public int fromY() { return Integer.min( from.y, to.y ); }
}

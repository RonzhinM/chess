package Chess.Players;

import Chess.ChessBoard;
import Chess.Color;
import Chess.Move;
import jdk.jshell.spi.ExecutionControl;

public abstract class Player {
    public final String name;
    public final Color color;
    protected ChessBoard chessBoard;
    public Player(String name, Color color) {
        this.name = name;
        this.color = color;
    }
    public void setChessBoard(ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
    }
    public abstract Move makeChoose() throws ExecutionControl.NotImplementedException;
    @Override
    public String toString() {
        return name;
    }
}